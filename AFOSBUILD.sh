rm -rf /opt/ANDRAX/wapiti

WORKDIR=$(pwd)

python3 -m venv /opt/ANDRAX/wapiti

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Create Virtual Environment... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $WORKDIR

source /opt/ANDRAX/wapiti/bin/activate

/opt/ANDRAX/wapiti/bin/pip3 install wheel msgpack ujson setuptools_rust

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install basic setup... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/wapiti/bin/pip3 install .

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install wapiti module... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
